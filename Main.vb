Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On
Imports Console = System.Console

Public Module Main

    Public Sub Main()
        Try
            Dim N As Integer = CInt(Console.ReadLine())
            Dim roses(N - 1) As String
            For i As Integer = 0 To UBound(roses)
                roses(i) = Console.ReadLine()
            Next i
            Dim NP As Integer = CInt(Console.ReadLine())

            Dim cs As New CakeSharing()
            Dim ret() As String = cs.cut(roses, NP)

            Console.WriteLine(ret.Length)
            For Each r As String In ret
                Console.WriteLine(r)
            Next r
            Console.Out.Flush()

        Catch Ex As Exception
            Console.Error.WriteLine(ex)
        End Try
    End Sub

End Module