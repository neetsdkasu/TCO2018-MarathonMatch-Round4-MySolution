Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On

Imports System
Imports System.Collections.Generic
Imports Tp = System.Tuple(Of Integer, Integer)
Imports TpDII = System.Tuple(Of Double, Integer, Integer)
Imports TpDI = System.Tuple(Of Double, Integer)
Imports TpIII = System.Tuple(Of Integer, Integer, Integer)
Imports TpDIs = System.Tuple(Of Double, Integer())
Imports TpDIsI = System.Tuple(Of Double, Integer(), Integer)

Public Class CakeSharing
    Dim startTime As Integer, limitTime As Integer
    Dim rand As New Random(19831983)
    Dim H, W, NP, RC As Integer
    Dim roses(,) As Boolean
    Dim rosesHSum(,) As Integer
    Dim rosesWSum(,) As Integer

    Public Function cut(rosesStr() As String, NP As Integer) As String()
        startTime = Environment.TickCount
        limitTime = startTime + 8500
        Console.Error.WriteLine("{0}, {1}", limitTime, rand.Next(0, 100))

        H = rosesStr.Length
        W = rosesStr(0).Length
        Me.NP = NP

        Console.Error.WriteLine("H: {0}, W: {1}, NP: {2}", H, W, NP)

        ReDim roses(W - 1, H - 1)
        RC = 0
        For i As Integer = 0 To H - 1
            For j As Integer = 0 To W - 1
                If rosesStr(i)(j) = "R"c Then
                    roses(j, i) = True
                    RC += 1
                End If
            Next j
        Next i
        ReDim rosesHSum(W - 1, H)
        For i As Integer = 0 To H - 1
            For j As Integer = 0 To W - 1
                rosesHSum(j, i + 1) = rosesHSum(j, i) + If(roses(j, i), 1, 0)
            Next j
        Next i
        ReDim rosesWSum(W, H - 1)
        For i As Integer = 0 To H - 1
            For j As Integer = 0 To W - 1
                rosesWSum(j + 1, i) = rosesWSum(j, i) + If(roses(j, i), 1, 0)
            Next j
        Next i

        Dim ret() As String = Nothing

        Dim ss1 As Tuple(Of Double, String()) = GetIdea2()
        If ss1 IsNot Nothing Then
            ret = ss1.Item2
        End If
        Dim ss2 As Tuple(Of Double, String()) = GetIdea2H()
        If ss2 IsNot Nothing Then
            If ret Is Nothing OrElse ss1 Is Nothing Then
                ret = ss2.item2
            ElseIf ss2.Item1 < ss1.Item1 Then
                ret = ss2.Item2
            End If
        End If

        If ret Is Nothing OrElse ret.Length <> NP - 1 Then
            Console.Error.WriteLine("Oops!")
            If W Mod NP = 0 Then
                ret = GetWSplit()
            ElseIf H Mod NP = 0 Then
                ret = GetHSplit()
            Else
                ret = GetNoApproach()
            End If
        End If

        cut = ret

        Console.Error.WriteLine("total time: {0} ms", Environment.TickCount - startTime)

        ' For i As Integer = 0 To UBound(ret)
            ' Console.Error.WriteLine("{0}: {1}", i, ret(i))
        ' Next i

    End Function

    Private Function GetWSplit() As String()
        Dim ret(NP - 2) As String
        Dim r As New Rect(W, H)
        Dim dw As Integer = W \ NP
        For i As Integer = 0 To NP - 2
            Dim sp As Tuple(Of Rect, Rect, Line) = r.SplitV(dw)
            ret(i) = sp.Item3.ToString()
            r = sp.Item2
        Next i
        GetWSplit = ret
    End Function

    Private Function GetHSplit() As String()
        Dim ret(NP - 2) As String
        Dim r As New Rect(W, H)
        Dim dh As Integer = H \ NP
        For i As Integer = 0 To NP - 2
            Dim sp As Tuple(Of Rect, Rect, Line) = r.SplitH(dh)
            ret(i) = sp.Item3.ToString()
            r = sp.Item2
        Next i
        GetHSplit = ret
    End Function

    Private Function GetNoApproach() As String()
        Dim NP As Integer = Me.NP - 1
        Dim ret(NP - 1) As String
        Dim r As New Rect(W, H)
        Dim pos As Integer = 0
        Dim xs As New Queue(Of Rect)()
        Dim ys As New Queue(Of Rect)()
        xs.Enqueue(r)
        Do While NP > 0 AndAlso xs.Count > 0
            r = xs.Dequeue()
            Dim dv As Integer = r.sz.W \ 2
            Dim sp As Tuple(Of Rect, Rect, Line) = r.SplitV(dv)
            ret(pos) = sp.Item3.ToString()
            If sp.Item2.sz.W > 1 Then
                xs.Enqueue(sp.Item2)
            Else
                ys.Enqueue(sp.Item2)
            End If
            If sp.Item1.sz.W > 1 Then
                xs.Enqueue(sp.Item1)
            Else
                ys.Enqueue(sp.Item1)
            End If
            pos += 1
            NP -= 1
        Loop
        Do While NP > 0 AndAlso ys.Count > 0
            xs.Enqueue(ys.Dequeue())
        Loop
        Do While NP > 0 AndAlso xs.Count > 0
            r = xs.Dequeue()
            Dim dv As Integer = r.sz.H \ 2
            Dim sp As Tuple(Of Rect, Rect, Line) = r.SplitH(dv)
            ret(pos) = sp.Item3.ToString()
            If sp.Item2.sz.H > 1 Then xs.Enqueue(sp.Item2)
            If sp.Item1.sz.H > 1 Then xs.Enqueue(sp.Item1)
            pos += 1
            NP -= 1
        Loop
        GetNoApproach = ret
    End Function


    Class Point
        Public ReadOnly X, Y As Integer
        Sub New(X As Integer, Y As Integer)
            Me.X = X: Me.Y = Y
        End Sub
        Function Move(dX As Integer, dY As Integer) As Point
            Move = New Point(X + dX, Y + dY)
        End Function
        Public Overrides Function ToString() As String
            ToString = String.Format("{0} {1}", X, Y)
        End Function
    End Class

    Class Size
        Public ReadOnly W, H As Integer
        Sub New(W As Integer, H As Integer)
            Me.W = W: Me.H = H
        End Sub
        Function Resize(dW As Integer, dH As Integer) As Size
            Resize = New Size(W + dW, H + dH)
        End Function
        Function ResizeW(nW As Integer) As Size
            ResizeW = New Size(nW, H)
        End Function
        Function ResizeH(nH As Integer) As Size
            ResizeH = New Size(W, nH)
        End Function
        Public Overrides Function ToString() As String
            ToString = String.Format("{0} {1}", W, H)
        End Function
    End Class

    Class Line
        Public ReadOnly p0, p1 As Point
        Sub New(p0 As Point, p1 As Point)
            Me.p0 = p0: Me.p1 = p1
        End Sub
        Public Overrides Function ToString() As String
            ToString = p0.ToString() + " " + p1.ToString()
        End Function
    End Class

    Class Rect
        Public ReadOnly p As Point
        Public ReadOnly sz As Size
        Sub New(W As Integer, H As Integer)
            p = New Point(0, 0): sz = New Size(W, H)
        End Sub
        Sub New(p As Point, sz As Size)
            Me.p = p: Me.sz = sz
        End Sub
        Function SplitV(c As Integer) As Tuple(Of Rect, Rect, Line)
            If sz.W = 1 Then Throw New Exception("cannot SplitV")
            Dim r1 As New Rect(p, sz.ResizeW(c))
            Dim r2 As New Rect(p.Move(c, 0), sz.Resize(-c, 0))
            Dim ln As New Line(p.Move(c, 0), p.Move(c, sz.H))
            SplitV = New Tuple(Of Rect, Rect, Line)(r1, r2, ln)
        End Function
        Function SplitH(c As Integer) As Tuple(Of Rect, Rect, Line)
            If sz.H = 1 Then Throw New Exception("cannot SplitH")
            Dim r1 As New Rect(p, sz.ResizeH(c))
            Dim r2 As New Rect(p.Move(0, c), sz.Resize(0, - c))
            Dim ln As New Line(p.Move(0, c), p.Move(sz.W, c))
            SplitH = New Tuple(Of Rect, Rect, Line)(r1, r2, ln)
        End Function
        Public Overrides Function ToString() As String
            ToString = String.Format("Rect[ p: {0} sz: {1} ]", p.ToString(), sz.ToString())
        End Function
    End Class

    Private Function GetIdea2() As Tuple(Of Double, String())
        GetIdea2 = Nothing
        Dim z As Double = CDbl(W * H) / CDbl(NP)
        Dim bigcuts As New List(Of TpDII())()
        Dim bcidx As New List(Of TpDI)()
        For dv As Integer = 1 To Math.Min(W, NP)
            If dv * (H - 1) + (dv - 1) < (NP - 1) Then Continue For
            Dim dW As Integer = W \ dv
            Dim rd As Integer = (NP - 1) - (dv - 1)
            Dim cdH As Integer = rd \ dv
            Dim mdW As Integer = W Mod dv
            Dim mcdH As Integer = rd Mod dv
            ' Console.Error.WriteLine("dv:{0} dw:{1} ({3}) rd:{2} cDH:{4} ({5})", dv, dw, rd, mdW, cdH, mcdH)
            Dim tmp(dv - 1) As TpDII
            For i As Integer = 0 To dv - 1
                Dim ww As Integer = If(i < mdW, dW + 1, dW)
                Dim ch As Integer = If(i < mcdH, cdH + 1, cdH)
                Dim sc As Double = CDbl(ww * H) / CDbl(ch + 1)
                tmp(i) = New TpDII(sc, ww, ch)
            Next i
            Do
                Array.Sort(tmp)
                Dim sm As TpDII = tmp(0)
                Dim bg As TpDII = tmp(dv - 1)
                Dim df As Double = bg.Item1 - sm.Item1
                Dim wa As Integer = sm.Item2 + bg.Item2 - 2
                Dim ca As Integer = sm.Item3 + bg.Item3
                Dim fg As Boolean = True
                For i As Integer = 0 To wa \ 2
                    Dim w1 As Integer = i + 1
                    Dim w2 As Integer = wa - i + 1
                    For j As Integer = 0 To ca \ 2
                        Dim c1 As Integer = j
                        Dim c2 As Integer = ca - j
                        If c1 >= H OrElse c2 >= H Then Continue For
                        Dim sc1 As Double = CDbl(w1 * H) / CDbl(c1 + 1)
                        Dim sc2 As Double = CDbl(w2 * H) / CDbl(c2 + 1)
                        Dim dft As Double = Math.Abs(sc2 - sc1)
                        If dft < df Then
                            df = dft
                            tmp(0) = New TpDII(sc1, w1, c1)
                            tmp(dv - 1) = New TpDII(sc2, w2, c2)
                            fg = False
                        End If
                    Next j
                Next i
                If fg Then Exit Do
            Loop
            Dim id As Integer = bigcuts.Count
            bigcuts.Add(tmp)
            Dim vsc As Double = 0
            For Each t As TpDII In tmp
                Dim ww As Integer = t.Item2
                Dim ch As Integer = t.Item3
                Dim hh As Integer = H \ (ch + 1)
                Dim mhh As Integer = H Mod (ch + 1)
                vsc += ((CDbl(ww * hh) - z) ^ 2) * CDbl(ch + 1 - mhh)
                vsc += ((CDbl(ww * (hh + 1)) - z) ^ 2) * CDbl(mhh)
            Next t
            bcidx.Add(New TpDI(vsc, id))
        Next dv

        If Environment.TickCount > limitTime Then Exit Function

        If bigcuts.Count = 0 Then Exit Function

        bcidx.Sort()

        Dim rs As Double = CDbl(RC) / CDbl(NP)

        Dim retBigls() As Tp = Nothing
        Dim retBigrs() As TpDIsI = Nothing
        Dim retSc As Double = Double.MaxValue

        Dim escTime As Integer = startTime + 4500

        For f As Integer = 0 To bcidx.Count - 1
            Dim time1 As Integer = Environment.TickCount
            If time1 > escTime Then
                Exit For
            End If

            Dim selSc As Double = Math.Sqrt(bcidx(f).Item1 / CDbl(NP)) + 1.0
            Dim sel() As TpDII = bigcuts(bcidx(f).Item2)

            Dim bigs As New Dictionary(Of Tp, Integer)()
            For Each t As TpDII In sel
                Dim k As New Tp(t.Item2, t.Item3)
                Dim c As Integer = 0
                If bigs.TryGetValue(k, c) Then
                    bigs(k) = c + 1
                Else
                    bigs.Add(k, 1)
                End If
            Next t

            Dim bigps() As Tp = bigs.Keys.ToArray()
            Dim bigls(UBound(sel)) As Tp
            Dim bigrs(UBound(sel)) As TpDIsI
            Dim x As Integer = 0
            Dim sc As Double = 0.0
            Dim tf As Integer = 0
            For i As Integer = 0 To UBound(bigls)
                Dim selP As Tp = Nothing
                Dim selPr As TpDIsI = Nothing
                For Each p As Tp In bigps
                    If bigs(p) = 0 Then Continue For
                    Dim pr As TpDIsI = Place(x, p, rs)
                    If pr Is Nothing Then Continue For
                    If selP Is Nothing OrElse pr.Item1 < selPr.Item1 Then
                        selP = p
                        selPr = pr
                    End If
                Next p
                If selP Is Nothing Then
                    Console.Error.WriteLine("what happened?")
                    Exit Function
                End If
                bigs(selP) -= 1
                bigls(i) = selP
                bigrs(i) = selPr
                x += selP.Item1
                sc += selPr.Item1
                tf += selPr.Item3
            Next i

            ' Dim ig As Integer = RC - tf
            ' sc += CDbl(ig) * CDbl(ig) / CDbl(NP)

            selSc *= Math.Sqrt(sc / CDbl(NP)) + 1.0

            If selSc >= retSc Then Continue For

            retSc = selSc
            retBigls = CType(bigls.Clone(), Tp())
            retBigrs = CType(bigrs.Clone(), TpDIsI())

        Next f

        If retBigls Is Nothing Then Exit Function

        Dim ret(NP - 2) As String
        Dim pos As Integer = 0
        ' Dim scr As Double = 0.0
        Dim xx = 0
        For i As Integer = 0 To UBound(retBigls)
            Dim dW As Integer = retBigls(i).Item1
            Dim ptL As Point
            Dim ptR As New Point(xx + dW, 0)
            Dim ln As New Line(ptR, ptR.Move(0, H))
            If xx + dW < W Then
                ret(pos) = ln.ToString()
                pos += 1
            End If
            Dim pr As TpDIsI = retBigrs(i)
            ' scr += pr.Item1
            Dim ss() As Integer = pr.Item2
            For j As Integer = 0 To UBound(ss) Step 2
                ptL = New Point(xx, ss(j))
                ptR = New Point(xx + dW, ss(j + 1))
                ln = New Line(ptL, ptR)
                ret(pos) = ln.ToString()
                pos += 1
            Next j
            xx += dW
        Next i

        GetIdea2 = New Tuple(Of Double, String())(retSc, ret)
    End Function

    Class Node
        Public ReadOnly v As TpIII
        Public ReadOnly n As Node
        Sub New(v As TpIII, n As Node)
            Me.v = v: Me.n = n
        End Sub
    End Class

    Private Function Place(offX As Integer, p As Tp, rs As Double) As TpDIsI
        Place = Nothing
        Dim dW As Integer = p.Item1
        Dim CDH As Integer = p.Item2
        Dim dH As Integer = H \ (CDH + 1)
        Dim s1 As Integer = dW * dH
        Dim s2 As Integer = dW * (dH + 1)
        Dim tmp As New Dictionary(Of TpIII, Tuple(Of TpDIs, Node, Integer))()
        Dim emp(dW - 1) As Integer
        tmp.Add(New TpIII(0, 0, H Mod (CDH + 1)), _
            New Tuple(Of TpDIs, Node, Integer)(New TpDIs(0.0, emp), Nothing, 0))
        For i As Integer = 0 To CDH - 1
            If Environment.TickCount > limitTime Then Exit Function
            Dim tmp2 As New Dictionary(Of TpIII, Tuple(Of TpDIs, Node, Integer))()
            For Each kv As KeyValuePair(Of TpIII, Tuple(Of TpDIs, Node, Integer)) In tmp
                Dim Li As Integer = kv.Key.Item1
                Dim Ri As Integer = kv.Key.Item2
                Dim Ci As Integer = kv.Key.Item3
                Dim sci As Double = kv.Value.Item1.Item1
                Dim ixs() As Integer = kv.Value.Item1.Item2
                Dim nd As New Node(kv.Key, kv.Value.Item2)
                Dim toc As Integer = kv.Value.Item3

                If Ci < CDH - i Then
                    For y0 As Integer = Li To Math.Min(H, Li + dH * 2)
                        Dim y1 As Integer = dH * 2 - (y0 - Li) + Ri
                        If y1 > H OrElse y1 < Ri Then Continue For
                        Dim nxs(dW - 1) As Integer
                        Dim c As Integer = 0
                        If y0 = y1 Then
                            For j As Integer = 0 To UBound(nxs)
                                Dim x As Integer = j + offX
                                c += rosesHSum(x, y0) - rosesHSum(x, ixs(j))
                                nxs(j) = y0
                            Next j
                            Dim sc As Double = ((CDbl(c) - rs) ^ 2) + sci
                            Dim ky As New TpIII(y0, y0, Ci)
                            Dim vl As Tuple(Of TpDIs, Node, Integer) = Nothing
                            If tmp2.TryGetValue(ky, vl) Then
                                If vl.Item1.Item1 <= sc Then Continue For
                            End If
                            Dim ep As Integer = toc + c
                            tmp2(ky) = New Tuple(Of TpDIs, Node, Integer)(New TpDIs(sc, nxs), nd, ep)
                            Continue For
                        End If

                        Dim k As Double = CDbl(y1 - y0) / CDbl(dW)
                        For x As Integer = 0 To dW - 1
                            Dim yd As Double = k * (CDbl(x) + 0.5) + CDbl(y0)
                            Dim yi As Integer = CInt(Math.Floor(yd))
                            Dim ye As Double = CDbl(yi) + 0.5
                            Dim cm As Integer = yd.CompareTo(ye)
                            If cm < 0 Then
                                nxs(x) = Math.Min(H, Math.Max(ixs(x), yi))
                            ElseIf cm > 0 Then
                                nxs(x) = Math.Min(H, Math.Max(ixs(x), yi + 1))
                            Else
                                nxs(x) = Math.Min(H, Math.Max(ixs(x), yi + 1))
                                c -= 1
                            End If
                        Next x
                        For j As Integer = 0 To UBound(nxs)
                            Dim x As Integer = j + offX
                            c += rosesHSum(x, nxs(j)) - rosesHSum(x , ixs(j))
                        Next j
                        Dim sc1 As Double = ((CDbl(c) - rs) ^ 2) + sci
                        Dim ky1 As New TpIII(y0, y1, Ci)
                        Dim vl1 As Tuple(Of TpDIs, Node, Integer) = Nothing
                        If tmp2.TryGetValue(ky1, vl1) Then
                            If vl1.Item1.Item1 <= sc1 Then Continue For
                        End If
                        Dim ep1 As Integer = toc + c
                        tmp2(ky1) = New Tuple(Of TpDIs, Node, Integer)(New TpDIs(sc1, nxs), nd, ep1)
                    Next y0
                End If

                If Ci > 0 Then
                    For y0 As Integer = Li To Math.Min(H, Li + (dH + 1) * 2)
                        Dim y1 As Integer = (dH + 1) * 2 - (y0 - Li) + Ri
                        If y1 > H OrElse y1 < Ri Then Continue For

                        Dim nxs(dW - 1) As Integer
                        Dim c As Integer = 0
                        If y0 = y1 Then
                            For j As Integer = 0 To UBound(nxs)
                                Dim x As Integer = j + offX
                                c += rosesHSum(x, y0) - rosesHSum(x, ixs(j))
                                nxs(j) = y0
                            Next j
                            Dim sc As Double = ((CDbl(c) - rs) ^ 2) + sci
                            Dim ky As New TpIII(y0, y0, Ci - 1)
                            Dim vl As Tuple(Of TpDIs, Node, Integer) = Nothing
                            If tmp2.TryGetValue(ky, vl) Then
                                If vl.Item1.Item1 <= sc Then Continue For
                            End If
                            Dim ep As Integer = toc + c
                            tmp2(ky) = New Tuple(Of TpDIs, Node, Integer)(New TpDIs(sc, nxs), nd, ep)
                            Continue For
                        End If

                        Dim k As Double = CDbl(y1 - y0) / CDbl(dW)
                        For x As Integer = 0 To dW - 1
                            Dim yd As Double = k * (CDbl(x) + 0.5) + CDbl(y0)
                            Dim yi As Integer = CInt(Math.Floor(yd))
                            Dim ye As Double = CDbl(yi) + 0.5
                            Dim cm As Integer = yd.CompareTo(ye)
                            If cm < 0 Then
                                nxs(x) = Math.Min(H, Math.Max(ixs(x), yi))
                            ElseIf cm > 0 Then
                                nxs(x) = Math.Min(H, Math.Max(ixs(x), yi + 1))
                            Else
                                nxs(x) = Math.Min(H, Math.Max(ixs(x), yi + 1))
                                c -= 1
                            End If
                        Next x

                        For j As Integer = 0 To UBound(nxs)
                            Dim x As Integer = j + offX
                            c += rosesHSum(x, nxs(j)) - rosesHSum(x , ixs(j))
                        Next j
                        Dim sc1 As Double = ((CDbl(c) - rs) ^ 2) + sci
                        Dim ky1 As New TpIII(y0, y1, Ci - 1)
                        Dim vl1 As Tuple(Of TpDIs, Node, Integer) = Nothing
                        If tmp2.TryGetValue(ky1, vl1) Then
                            If vl1.Item1.Item1 <= sc1 Then Continue For
                        End If
                        Dim ep1 As Integer = toc + c
                        tmp2(ky1) = New Tuple(Of TpDIs, Node, Integer)(New TpDIs(sc1, nxs), nd, ep1)

                    Next y0
                End If

            Next kv
            tmp = tmp2
        Next i

        If tmp.Count = 0 Then Exit Function

        Dim selSc As Double = Double.MaxValue
        Dim selNd As Node = Nothing
        Dim selEp As Integer = 0

        For Each kv As KeyValuePair(Of TpIII, Tuple(Of TpDIs, Node, Integer)) In tmp
            ' Console.Error.WriteLine("{0}", kv)
            Dim sc As Double = kv.Value.Item1.Item1
            Dim c As Integer = 0
            Dim ixs() As Integer = kv.Value.Item1.Item2
            For i As Integer = 0 To UBound(ixs)
                Dim x As Integer = i + offX
                c += rosesHSum(x, H) - rosesHSum(x, ixs(i))
            Next i
            sc += (CDbl(c) - rs) ^ 2
            If sc >= selSc Then Continue For
            selSc = sc
            selNd = New Node(kv.Key, kv.Value.Item2)
            selEp = kv.Value.Item3
        Next kv

        If selNd Is Nothing Then Exit Function

        Dim ls(CDH * 2 - 1) As Integer
        Dim pos As Integer = 0
        Do While selNd.n IsNot Nothing
            ls(pos) = selNd.v.Item1
            ls(pos + 1) = selNd.v.Item2
            pos += 2
            selNd = selNd.n
        Loop

        Place = New TpDIsI(selSc, ls, selEp)

    End Function


    Private Function GetIdea2H() As Tuple(Of Double, String())
        GetIdea2H = Nothing
        If Environment.TickCount > limitTime Then Exit Function
        Dim z As Double = CDbl(W * H) / CDbl(NP)
        Dim bigcuts As New List(Of TpDII())()
        Dim bcidx As New List(Of TpDI)()
        For dv As Integer = 1 To Math.Min(H, NP)
            If dv * (H - 1) + (dv - 1) < (NP - 1) Then Continue For
            Dim dH As Integer = H \ dv
            Dim rd As Integer = (NP - 1) - (dv - 1)
            Dim cdW As Integer = rd \ dv
            Dim mdH As Integer = H Mod dv
            Dim mcdW As Integer = rd Mod dv
            ' Console.Error.WriteLine("dv:{0} dw:{1} ({3}) rd:{2} cDH:{4} ({5})", dv, dw, rd, mdW, cdH, mcdH)
            Dim tmp(dv - 1) As TpDII
            For i As Integer = 0 To dv - 1
                Dim hh As Integer = If(i < mdH, dH + 1, dH)
                Dim cw As Integer = If(i < mcdW, cdW + 1, cdW)
                Dim sc As Double = CDbl(hh * W) / CDbl(cw + 1)
                tmp(i) = New TpDII(sc, hh, cw)
            Next i
            Do
                Array.Sort(tmp)
                Dim sm As TpDII = tmp(0)
                Dim bg As TpDII = tmp(dv - 1)
                Dim df As Double = bg.Item1 - sm.Item1
                Dim ha As Integer = sm.Item2 + bg.Item2 - 2
                Dim ca As Integer = sm.Item3 + bg.Item3
                Dim fg As Boolean = True
                For i As Integer = 0 To ha \ 2
                    Dim h1 As Integer = i + 1
                    Dim h2 As Integer = ha - i + 1
                    For j As Integer = 0 To ca \ 2
                        Dim c1 As Integer = j
                        Dim c2 As Integer = ca - j
                        If c1 >= W OrElse c2 >= W Then Continue For
                        Dim sc1 As Double = CDbl(h1 * W) / CDbl(c1 + 1)
                        Dim sc2 As Double = CDbl(h2 * W) / CDbl(c2 + 1)
                        Dim dft As Double = Math.Abs(sc2 - sc1)
                        If dft < df Then
                            df = dft
                            tmp(0) = New TpDII(sc1, h1, c1)
                            tmp(dv - 1) = New TpDII(sc2, h2, c2)
                            fg = False
                        End If
                    Next j
                Next i
                If fg Then Exit Do
            Loop
            Dim id As Integer = bigcuts.Count
            bigcuts.Add(tmp)
            Dim vsc As Double = 0
            For Each t As TpDII In tmp
                Dim hh As Integer = t.Item2
                Dim cw As Integer = t.Item3
                Dim ww As Integer = W \ (cw + 1)
                Dim mww As Integer = W Mod (cw + 1)
                vsc += ((CDbl(hh * ww) - z) ^ 2) * CDbl(cw + 1 - mww)
                vsc += ((CDbl(hh * (ww + 1)) - z) ^ 2) * CDbl(mww)
            Next t
            bcidx.Add(New TpDI(vsc, id))
        Next dv

        If Environment.TickCount > limitTime Then Exit Function
        If bigcuts.Count = 0 Then Exit Function

        bcidx.Sort()

        Dim rs As Double = CDbl(RC) / CDbl(NP)

        Dim retBigls() As Tp = Nothing
        Dim retBigrs() As TpDIsI = Nothing
        Dim retSc As Double = Double.MaxValue

        Dim escTime As Integer = startTime + 6500

        For f As Integer = 0 To bcidx.Count - 1
            Dim time1 As Integer = Environment.TickCount
            If time1 > escTime Then
                Exit For
            End If

            Dim selSc As Double = Math.Sqrt(bcidx(f).Item1 / CDbl(NP)) + 1.0
            Dim sel() As TpDII = bigcuts(bcidx(f).Item2)

            Dim bigs As New Dictionary(Of Tp, Integer)()
            For Each t As TpDII In sel
                Dim k As New Tp(t.Item2, t.Item3)
                Dim c As Integer = 0
                If bigs.TryGetValue(k, c) Then
                    bigs(k) = c + 1
                Else
                    bigs.Add(k, 1)
                End If
            Next t

            Dim bigps() As Tp = bigs.Keys.ToArray()
            Dim bigls(UBound(sel)) As Tp
            Dim bigrs(UBound(sel)) As TpDIsI
            Dim y As Integer = 0
            Dim sc As Double = 0.0
            Dim tf As Integer = 0
            For i As Integer = 0 To UBound(bigls)
                Dim selP As Tp = Nothing
                Dim selPr As TpDIsI = Nothing
                For Each p As Tp In bigps
                    If bigs(p) = 0 Then Continue For
                    Dim pr As TpDIsI = PlaceH(y, p, rs)
                    If pr Is Nothing Then Continue For
                    If selP Is Nothing OrElse pr.Item1 < selPr.Item1 Then
                        selP = p
                        selPr = pr
                    End If
                Next p
                If selP Is Nothing Then
                    Console.Error.WriteLine("what happened?")
                    Exit Function
                End If
                bigs(selP) -= 1
                bigls(i) = selP
                bigrs(i) = selPr
                y += selP.Item1
                sc += selPr.Item1
                tf += selPr.Item3
            Next i

            ' Dim ig As Integer = RC - tf
            ' sc += CDbl(ig) * CDbl(ig) / CDbl(NP)

            selSc *= Math.Sqrt(sc / CDbl(NP)) + 1.0

            If selSc >= retSc Then Continue For

            retSc = selSc
            retBigls = CType(bigls.Clone(), Tp())
            retBigrs = CType(bigrs.Clone(), TpDIsI())

        Next f

        If retBigls Is Nothing Then Exit Function

        Dim ret(NP - 2) As String
        Dim pos As Integer = 0
        ' Dim scr As Double = 0.0
        Dim yy = 0
        For i As Integer = 0 To UBound(retBigls)
            Dim dH As Integer = retBigls(i).Item1
            Dim ptL As Point
            Dim ptR As New Point(0, yy + dH)
            Dim ln As New Line(ptR, ptR.Move(W, 0))
            If yy + dH < H Then
                ret(pos) = ln.ToString()
                pos += 1
            End If
            Dim pr As TpDIsI = retBigrs(i)
            ' scr += pr.Item1
            Dim ss() As Integer = pr.Item2
            For j As Integer = 0 To UBound(ss) Step 2
                ptL = New Point(ss(j), yy)
                ptR = New Point(ss(j + 1), yy + dH)
                ln = New Line(ptL, ptR)
                ret(pos) = ln.ToString()
                pos += 1
            Next j
            yy += dH
        Next i

        GetIdea2H = New Tuple(Of Double, String())(retSc, ret)
    End Function


    Private Function PlaceH(offY As Integer, p As Tp, rs As Double) As TpDIsI
        PlaceH = Nothing
        Dim dH As Integer = p.Item1
        Dim CDW As Integer = p.Item2
        Dim dW As Integer = W \ (CDW + 1)
        Dim s1 As Integer = dH * dW
        Dim s2 As Integer = dH * (dW + 1)
        Dim tmp As New Dictionary(Of TpIII, Tuple(Of TpDIs, Node, Integer))()
        Dim emp(dH - 1) As Integer
        tmp.Add(New TpIII(0, 0, W Mod (CDW + 1)), _
            New Tuple(Of TpDIs, Node, Integer)(New TpDIs(0.0, emp), Nothing, 0))
        For i As Integer = 0 To CDW - 1
            If Environment.TickCount > limitTime Then Exit Function
            Dim tmp2 As New Dictionary(Of TpIII, Tuple(Of TpDIs, Node, Integer))()
            For Each kv As KeyValuePair(Of TpIII, Tuple(Of TpDIs, Node, Integer)) In tmp
                Dim Li As Integer = kv.Key.Item1
                Dim Ri As Integer = kv.Key.Item2
                Dim Ci As Integer = kv.Key.Item3
                Dim sci As Double = kv.Value.Item1.Item1
                Dim ixs() As Integer = kv.Value.Item1.Item2
                Dim nd As New Node(kv.Key, kv.Value.Item2)
                Dim toc As Integer = kv.Value.Item3

                If Ci < CDW - i Then
                    For x0 As Integer = Li To Math.Min(W, Li + dW * 2)
                        Dim x1 As Integer = dW * 2 - (x0 - Li) + Ri
                        If x1 > W OrElse x1 < Ri Then Continue For
                        Dim nxs(dH - 1) As Integer
                        Dim c As Integer = 0
                        If x0 = x1 Then
                            For j As Integer = 0 To UBound(nxs)
                                Dim y As Integer = j + offY
                                c += rosesWSum(x0, y) - rosesWSum(ixs(j), y)
                                nxs(j) = x0
                            Next j
                            Dim sc As Double = ((CDbl(c) - rs) ^ 2) + sci
                            Dim ky As New TpIII(x0, x0, Ci)
                            Dim vl As Tuple(Of TpDIs, Node, Integer) = Nothing
                            If tmp2.TryGetValue(ky, vl) Then
                                If vl.Item1.Item1 <= sc Then Continue For
                            End If
                            Dim ep As Integer = toc + c
                            tmp2(ky) = New Tuple(Of TpDIs, Node, Integer)(New TpDIs(sc, nxs), nd, ep)
                            Continue For
                        End If

                        Dim k As Double = CDbl(x1 - x0) / CDbl(dH)
                        For y As Integer = 0 To dH - 1
                            Dim xd As Double = k * (CDbl(y) + 0.5) + CDbl(x0)
                            Dim xi As Integer = CInt(Math.Floor(xd))
                            Dim xe As Double = CDbl(xi) + 0.5
                            Dim cm As Integer = xd.CompareTo(xe)
                            If cm < 0 Then
                                nxs(y) = Math.Min(W, Math.Max(ixs(y), xi))
                            ElseIf cm > 0 Then
                                nxs(y) = Math.Min(W, Math.Max(ixs(y), xi + 1))
                            Else
                                nxs(y) = Math.Min(W, Math.Max(ixs(y), xi + 1))
                                c -= 1
                            End If
                        Next y

                        For j As Integer = 0 To UBound(nxs)
                            Dim y As Integer = j + offY
                            c += rosesWSum(nxs(j), y) - rosesWSum(ixs(j), y)
                        Next j
                        Dim sc1 As Double = ((CDbl(c) - rs) ^ 2) + sci
                        Dim ky1 As New TpIII(x0, x1, Ci)
                        Dim vl1 As Tuple(Of TpDIs, Node, Integer) = Nothing
                        If tmp2.TryGetValue(ky1, vl1) Then
                            If vl1.Item1.Item1 <= sc1 Then Continue For
                        End If
                        Dim ep1 As Integer = toc + c
                        tmp2(ky1) = New Tuple(Of TpDIs, Node, Integer)(New TpDIs(sc1, nxs), nd, ep1)
                    Next x0
                End If

                If Ci > 0 Then
                    For x0 As Integer = Li To Math.Min(W, Li + (dW + 1) * 2)
                        Dim x1 As Integer = (dW + 1) * 2 - (x0 - Li) + Ri
                        If x1 > W OrElse x1 < Ri Then Continue For

                        Dim nxs(dH - 1) As Integer
                        Dim c As Integer = 0
                        If x0 = x1 Then
                            For j As Integer = 0 To UBound(nxs)
                                Dim y As Integer = j + offY
                                c += rosesWSum(x0, y) - rosesWSum(ixs(j), y)
                                nxs(j) = x0
                            Next j
                            Dim sc As Double = ((CDbl(c) - rs) ^ 2) + sci
                            Dim ky As New TpIII(x0, x0, Ci - 1)
                            Dim vl As Tuple(Of TpDIs, Node, Integer) = Nothing
                            If tmp2.TryGetValue(ky, vl) Then
                                If vl.Item1.Item1 <= sc Then Continue For
                            End If
                            Dim ep As Integer = toc + c
                            tmp2(ky) = New Tuple(Of TpDIs, Node, Integer)(New TpDIs(sc, nxs), nd, ep)
                            Continue For
                        End If

                        Dim k As Double = CDbl(x1 - x0) / CDbl(dH)
                        For y As Integer = 0 To dH - 1
                            Dim xd As Double = k * (CDbl(y) + 0.5) + CDbl(x0)
                            Dim xi As Integer = CInt(Math.Floor(xd))
                            Dim xe As Double = CDbl(xi) + 0.5
                            Dim cm As Integer = xd.CompareTo(xe)
                            If cm < 0 Then
                                nxs(y) = Math.Min(W, Math.Max(ixs(y), xi))
                            ElseIf cm > 0 Then
                                nxs(y) = Math.Min(W, Math.Max(ixs(y), xi + 1))
                            Else
                                nxs(y) = Math.Min(W, Math.Max(ixs(y), xi + 1))
                                c -= 1
                            End If
                        Next y

                        For j As Integer = 0 To UBound(nxs)
                            Dim y As Integer = j + offY
                            c += rosesWSum(nxs(j), y) - rosesWSum(ixs(j), y)
                        Next j
                        Dim sc1 As Double = ((CDbl(c) - rs) ^ 2) + sci
                        Dim ky1 As New TpIII(x0, x1, Ci - 1)
                        Dim vl1 As Tuple(Of TpDIs, Node, Integer) = Nothing
                        If tmp2.TryGetValue(ky1, vl1) Then
                            If vl1.Item1.Item1 <= sc1 Then Continue For
                        End If
                        Dim ep1 As Integer = toc + c
                        tmp2(ky1) = New Tuple(Of TpDIs, Node, Integer)(New TpDIs(sc1, nxs), nd, ep1)

                    Next x0
                End If

            Next kv
            tmp = tmp2
        Next i

        If tmp.Count = 0 Then Exit Function

        Dim selSc As Double = Double.MaxValue
        Dim selNd As Node = Nothing
        Dim selEp As Integer = 0

        For Each kv As KeyValuePair(Of TpIII, Tuple(Of TpDIs, Node, Integer)) In tmp
            ' Console.Error.WriteLine("{0}", kv)
            Dim sc As Double = kv.Value.Item1.Item1
            Dim c As Integer = 0
            Dim ixs() As Integer = kv.Value.Item1.Item2
            For i As Integer = 0 To UBound(ixs)
                Dim y As Integer = i + offY
                c += rosesWSum(W, y) - rosesWSum(ixs(i), y)
            Next i
            sc += (CDbl(c) - rs) ^ 2
            If sc >= selSc Then Continue For
            selSc = sc
            selNd = New Node(kv.Key, kv.Value.Item2)
            selEp = kv.Value.Item3
        Next kv

        If selNd Is Nothing Then Exit Function

        Dim ls(CDW * 2 - 1) As Integer
        Dim pos As Integer = 0
        Do While selNd.n IsNot Nothing
            ls(pos) = selNd.v.Item1
            ls(pos + 1) = selNd.v.Item2
            pos += 2
            selNd = selNd.n
        Loop

        PlaceH = New TpDIsI(selSc, ls, selEp)

    End Function

End Class